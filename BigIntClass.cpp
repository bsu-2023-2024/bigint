#include "BigIntClass.h"
#include <iostream>


BigIntClass::BigIntClass(const std::string& number)
{
	//this->digits = digits;
	this->numLength = number.size();

	if (number.size() > maxLength)
	{
		throw "The value shouldn't be longer than 100 digits";
	}

	int j = numLength - 1;
	for (size_t i = maxLength - 1; i > 0; i--)
	{
		digits[i] = number[j] - '0';
		if (j == 0)
		{
			break;
		}
		j--;
	}
}


void BigIntClass::print() const
{
	for (int i = maxLength - numLength; i < maxLength; i++) {
		std::cout << digits[i];
	}
	std::cout << std::endl;
}

void BigIntClass::add(BigIntClass& other)
{
	for (size_t i = maxLength - 1; i > 1; i--)
	{
		if (digits[i] + other.digits[i] > 9)
		{
			int tmp = digits[i];
			digits[i] = (tmp + other.digits[i]) % 10;
			digits[i - 1] += (tmp + other.digits[i]) / 10;
		}
		else
		{
			digits[i] = digits[i] + other.digits[i];
		}
	}
}

void BigIntClass::subtract(BigIntClass& other)
{
	for (size_t i = maxLength - 1; i > 1; i--)
	{
		if (digits[i] - other.digits[i] < 0)
		{
			int j = 1;
			while (digits[i - j] == 0)
			{
				j++;
			}
			digits[i] += 10;
			digits[i - j]--;

			while (j > 1)
			{
				j--;
				digits[i - j] += 9;
			}
		}
		int tmp = digits[i];
		digits[i] = tmp - other.digits[i];
	}
}

void BigIntClass::muliply(BigIntClass&)
{
}
