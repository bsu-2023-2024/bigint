#pragma once
#include <string>


const int maxLength = 100;

class BigIntClass
{
public:
	int digits[maxLength] = { 0 };
	int numLength;

	BigIntClass(const std::string& number);
	void print() const;
	void add(BigIntClass&);
	void subtract(BigIntClass&);
	void muliply(BigIntClass&);
	void devide(BigIntClass&);
private:
	
};

